# workshop-1

# workshop-1

Prerequisites: 

Installed node on laptop
Have a working AWS account 
https://serverless.com/framework/docs/providers/aws/guide/credentials/
Configured aws cli 
https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
open ~/.aws/credentials
add your access_key and secret_key



git clone git@bitbucket.org:hotelinking_team/workshop-1.git
git checkout -b your_name


------------------------------------------------
### STEP-1
------------------------------------------------

npm init -y 
npm i serverless 
touch serverless.yml
https://serverless.com/framework/docs/providers/aws/guide/intro/

```
service: validationService
provider:
  name: aws
  runtime: nodejs12.x
  stage: dev
  profile: your-profile-name
  region: eu-west-1

```

add 

```
functions:
  validate: # A Function
    handler: validate.validate
    events: # The Events that trigger this Function
      - http: 
          path: /validate
          method: post
```

------------------------------------------------
### STEP-2
------------------------------------------------

touch validate.js

```
'use strict';

module.exports.validate = async event => {
};

```

sls deploy 
sls info

check postman 
go to cloudformation look at stack

call your endpoint: https://2uif6mbu61.execute-api.eu-west-1.amazonaws.com/dev/validate?email=test234@test.com

copy result of body to .mocks/email.json

------------------------------------------------
### STEP-3
------------------------------------------------
npm i axios

add env to serverless.yml
```
    environment:
        kickboxKey: test_895e07b41af974e8dd099839651cd7517e58db0cda5557480a6bee300648ccf7
```

validate.js

```
const axios = require('axios');

module.exports.validate = async event => {
  
  const email = event.queryStringParameters.email;
  const key = process.env.kickboxKey;

  const url = `https://api.kickbox.com/v2/verify?email=${email}&apikey=${key}`;
  const response = await axios.get(url)

  return {
    statusCode: 200,
    body: JSON.stringify( response, null, 2 )
  };
};
```

------------------------------------------------
### STEP-4
------------------------------------------------

 npm i -D serverless-webpack webpack webpack-node-externals babel-loader
 https://github.com/serverless-heaven/serverless-webpack

update serverless.yml to include webpack plugin
and custom webpack configuration (not needed as same as default)
create webpack.config.js
sls deploy and check size of lambda in cloudformation

create launch.json file for debugging 
debug


------------------------------------------------
### STEP-5
------------------------------------------------
npm i serverless-offline
https://github.com/dherault/serverless-offline
add to serverless.yml (after webpack)
serverless offline go to localhost
add command to launch.json 
```  {
      "type": "node",
      "request": "launch",
      "name": "Offline", 
      "program": "${workspaceFolder}/node_modules/serverless/bin/serverless",
      "args": [ "offline"],
      "sourceMaps": true,
      "runtimeArgs": [
        "--lazy"
      ],
      "outFiles": [
        "${workspaceFolder}/.webpack/**/*.js"
      ],
      "protocol": "inspector",
      "runtimeExecutable": "node",
      "env": {
        // Here we set some environment vars that should be set locally.
        // They can and will overwrite the ones coming from your serverless.yml
      }
    }
```
to do live debugging

------------------------------------------------
### STEP-6
------------------------------------------------

npm i -D serverless-dynamodb-local
https://github.com/99xt/serverless-dynamodb-local/tree/v1
sls dynamodb install (install dynamodb locally java package)
add dynamodb table resource to serverless.yml
sls offline start

------------------------------------------------
### STEP-LAST
------------------------------------------------

sls remove -v (verbose)

